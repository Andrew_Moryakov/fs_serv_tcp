﻿#region usings

using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
 
using System.Linq;
using System.Windows.Media;
using System.Windows.Shapes;
 
using System.Text;
using FromCaffeModule;
#endregion
namespace AmurTcp
{ 
    //class ServerDP:FromCaffeModule.ServerDP{}
    /// <summary>
    /// Страница - интерфейс для взаимодействия с сервером
    /// </summary>
    public partial class HomePage : UserControl
    {
        #region Инициализация, обработчики, вспомогательные методы

     
        public HomePage()
        {

            InitializeComponent();
            Thread.Sleep(100);

            for (int i = 0; i <= 5; i++)
            {//Заполняем лист бокс пустыми данными
                listBox1.Items.Add(new ListViewItem());
            }

            for (int i = 0; i <=5; i++)
            {//Заполняем серыми лампачками
                ClientStatus_OnOff(i, Colors.LightGray);
            }
            ServerStatus_OnOff(Colors.LightGray, "Отключён");//Сам сервер - серая лампачка

            //Сохрраняем текущее состояние стрнаницы
          // UIcs.homePagesCollection.Add(this);

            
        }

        Thread WaitingForConnecting;
        
        ServerDP _server;

        public delegate void Task_Delegate(int s);
        /// <summary>
        /// Обработчик новых подключений к сервреу
        /// </summary>
        /// <param name="number">Номер клиента</param>
        void NewClient_Connecting(String number)
        {
            
            ClientStatus_OnOff(int.Parse(number), Colors.LawnGreen);
        }

        /// <summary>
        /// Возникает, когда сервер начал слушать клиентов
        /// </summary>
        /// <param name="flag">Истина слушает, ложь - не слушает</param>
        public void Server_is_OnlineOfline(Boolean flag)
        {
            if (flag)
            {
                ServerStatus_OnOff( Colors.LawnGreen, "Подключён" );
             
                
            }
            else
            {
                ServerStatus_OnOff( Colors.LightGray, "Отключён" );
              //  label1Status_Copy.Dispatcher.Invoke( new myDel( s => label1Status_Copy.Content = s ), "Отключен" );
           
                this.Dispatcher.Invoke(new Action(delegate
            {//разрушаем
                ServerOut();
                
            }));
            }
        }

          
        Dictionary<int, List<string>> clientsAndCarts = new Dictionary<int, List<string>>()
        {//Клиент и его заказы
            {0, new List<String>()},
             {1, new List<String>()},
             {2, new List<String>()},
             {3, new List<String>()},
             {4, new List<String>()},
             {5, new List<String>()}
        };

        private List<String> SendCommentsToClietn(Int32 numberB)
        {
            return DataBaceWork.GetCommentsFrom_id(numberB);//Получаем комменты по нужному продукту/ извлечение комментарие это уже задача не модуля, ему, лищь, останеться передать их клиенту
        }
        /// <summary>
        /// Пришел новый заказ
        /// </summary>
        /// <param name="datas">Пришедгие данные</param>
        String NewCart_Come(String numberTable, List<string> listData)
        {//Свое дело модуль сделал - сказал, кто заказывает блюдо, id блюд в БД. 
            
            try
            {


                var listOrders = DataBaceWork.GetOrdersFormDB(listData.Select(el => Int32.Parse(el)).ToList());

                    foreach (var el in listOrders)
                    {
                        clientsAndCarts[Int32.Parse(numberTable) - 1].Add(el);//Записываем клиенту его зазказы
                    }

                    CartStatus(Int32.Parse(numberTable) - 1, Brushes.CadetBlue); 

                return "Заказ принят. Ждите!";


            }
            catch (Exception ex)
            {
                return "Что то пошло не так, мы не получили ваш заказ! Обратитесь за помошью!" ;
                
            }
            //try
            //{
            //    label1Status.Dispatcher.Invoke(new myDel(s => label1Status.Content = s), "Заказ, успшно записанн в базу данных");
            //}
            //catch (Exception ex)
            //{
            //    label1Status.Dispatcher.Invoke(new myDel(s => label1Status.Content = s), "Ошибка записи в базу данных" + Environment.NewLine + ex.ToString());
            //}
        }
        delegate void myDel(string text);//для того, чтобы отображать данные на форме

        /// <summary>
        /// ПРишли комментарии для продукта
        /// </summary>
        /// <param name="data">Данные: номер продукта, комментарии</param>
        String Comments_isCame(String listData, int idDish)
        {
          
                CommentsDisplay(richTextboxComments, new List<String>(){ listData});//отображаем комментарии на экран
                DataBaceWork.AddComments_forId(idDish, listData);
            
                try
                {
                label1Status.Dispatcher.Invoke(new myDel(s => label1Status.Content = s), "Комментарии, успшно записанны в базу данных");

            }
            catch (Exception ex)
            {
                label1Status.Dispatcher.Invoke(new myDel(s => label1Status.Content = s), "Ошибка записи в базу данных");
            }
                return "Спасибо за Ваш отзыв!";
        }


        void Get_Cart_status( String number, string cart )
        {
            MessageBox.Show("Стол " + number +" хочет узнать статус " + cart);
        }

        /// <summary>
        /// Возникает, когда клиент отключился
        /// </summary>
        /// <param name="number">Номер стола</param>
        void Client_Disconnection(String number)
        {
            ClientStatus_OnOff(int.Parse(number), Colors.LightGray);//Сервер серая лампачка
            //OrdersDisplay(richTextboxClients, new AppData.ListXMLOfType(), "Отключился стол: " + number);
        }
        


        private void CommentsToDataBase(List<string> listDatas)
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// Подключение сервера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1Go_Click_1(object sender, RoutedEventArgs e)
        {

            _server = new ServerDP(ServerDP.ThisIp, ServerDP.ThisPort);//Экземпляр нашего сервера

            _server.onClient_Connect += NewClient_Connecting;
            _server.onClient_Disconnect += Client_Disconnection;
            _server.onNewCart_Come += NewCart_Come;
            _server.onComments_isCame += Comments_isCame;
            _server.onStatusServer += Server_is_OnlineOfline;//Подключения к событиям
            _server.onGetComments += SendCommentsToClietn;
            _server.onServer_err += Server_err;
            _server.onClient_cartStatus += Get_Cart_status;

            WaitingForConnecting = new System.Threading.Thread(new ThreadStart(_server.ServerListen));//Запускаем прослушивание порта - сервером
            WaitingForConnecting.Start();//Запускаем поток, передаем данные для подключения

            button1Go.IsEnabled = false;//отключаем кнопку
            buttonOut.IsEnabled = true;
           // UIcs.homePagesCollection.Add(this);//Запоминаем главную страницу, на которую можно вернуться, так как состояния контролов изменились то сохраняется именно эта копия/ так как this is readonly то скорее всего создается копия объекта, а не ссылка

        }
 void Server_err(String err, Exception ex=null)
 {

     MessageBox.Show(err, "Ошибка сервера", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.ServiceNotification);
  //   WaitingForConnecting.Abort();
     
     //if (_server.Server_Recovery())
     //{
         this.Dispatcher.Invoke(new Action(delegate
          {
              button1Go.IsEnabled = false;//отключаем кнопку
              buttonOut.IsEnabled = true;
          }));
     
        // UIcs.homePagesCollection[0] = this;
      //  MessageBox.Show("Произошла ошибка, сервер востоновил свою работоспособность", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.None, MessageBoxOptions.DefaultDesktopOnly);
     //}
     //else
     //{
 
     //        MessageBox.Show("Произошла ошибка, и работоспособность востановить не удалось", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.None, MessageBoxOptions.DefaultDesktopOnly);
       
     //    this.Dispatcher.Invoke(new Action(delegate
     //    {
     //       ServerOut();
     //        label1Status.Dispatcher.Invoke(new myDel(s => label1Status.Content = s), "Офлайн");
     //     //   WaitingForConnecting.Abort();
     //    }));
        
     //    return;
     //}


 }
        /// <summary>
        /// Отображает на экран данные
        /// </summary>
        /// <param name="otherData">Список данных, не обязательный параметр</param>
        /// <param name="richTB">Куда записывать</param>
        /// <param name="listDatas">Еденичные данные</param>
        private void CartToDisplay(String numberTable, List<String> listOrders,   Control richTB )
        {//Отображаем на форму какие данные 

           // listOrders[0] = "Стол " + numberTable + Environment.NewLine + listOrders[0];
            foreach (String el in listOrders)
                {//Выбирвем из тега <data/>

                    //Выводим в текст бокс. какие то данные (заказ или клиентов)
                    ((richTB) as RichTextBox).Dispatcher.Invoke(new myDel(s => ((richTB) as RichTextBox).AppendText(s + Environment.NewLine)), el);
                }
 
        }

        /// <summary>
        /// Отображает на экран данные
        /// </summary>
        /// <param name="otherData">Список данных, не обязательный параметр</param>
        /// <param name="richTB">Куда записывать</param>
        /// <param name="listDatas">Еденичные данные</param>
        private void CommentsDisplay(Control richTB, List<String> listComments)
        {//Отображаем на форму koments
           // listDatas.list[0] = "Комментарии продукта " + listDatas.numbersTable + ": " + Environment.NewLine + listDatas.list[0] + Environment.NewLine;
            foreach (String el in listComments)
            {//Выбирвем из тега <data/>

                //Выводим в текст бокс. какие то данные (заказ или клиентов)
                ((richTB) as RichTextBox).Dispatcher.Invoke(new myDel(s => ((richTB) as RichTextBox).AppendText(s)), el);
            }


        }

        /// <summary>
        /// Отключаем сервер
        /// </summary>
        void ServerOut()
        {

            button1Go.IsEnabled = true;
            buttonOut.IsEnabled = false;
            Thread.Sleep(300);

            for (int i = 0; i <5; i++)
            {
                ClientStatus_OnOff(i, Colors.LightGray);
            }
            ServerStatus_OnOff( Colors.LightGray, "Отключён" );
        }

        /// <summary>
        /// Отключение сервера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOut_Click(object sender, RoutedEventArgs e)
        {

            _server.ServerOut();

            ServerOut();
            WaitingForConnecting.Abort();
            ServerStatus_OnOff( Colors.LightGray, "Отключён" );
        }

        /// <summary>
        /// Меняем лапочки для клиентов
        /// </summary>
        /// <param name="n">Номер стола</param>
        /// <param name="clr">Цвет лампочки</param>
        void ClientStatus_OnOff(int n, Color clr)
        {
            this.Dispatcher.Invoke(new Action(delegate
            {
                
                StackPanel stp = new StackPanel() { Margin = new Thickness(0.0, 10.0, 0.0, 0.0) };//Панелька, содержит текст и кружочек
                // Create a red Ellipse.
                Ellipse myEllipse = new Ellipse()
                {//Кружочек

                    Fill = new SolidColorBrush(clr),
                    StrokeThickness = 0,
                    Stroke = Brushes.Black,
                    Width = 10,
                    Height = 10,
                    Margin = new Thickness(45.0, -25.0, 0.0, 0.0)
                };

                Label lbl = new Label()
                {//Текст
                    Content = "Стол " + Convert.ToInt32(n+1),
                    Margin = new Thickness(0.0, -12.0, 0.0, 0.0)
                };
                ListViewItem item = new ListViewItem();// { Margin = new Thickness(0.0, 10.0, 0.0, 0.0) };

                stp.Children.Add(lbl);//Add text
                stp.Children.Add(myEllipse);//Add ellipse
                item.Content = stp;//Add [text and ellopse] to anyone item for ListBox


                this.listBox1.Items[n] = item;//Add item to ListBox
            }));

        }

        /// <summary>
        /// Меняем лампачку сервера
        /// </summary>
        /// <param name="clr">Цвет</param>
        void ServerStatus_OnOff(Color clr, String text)
        {
            this.Dispatcher.Invoke(new Action(delegate
            {

                StackPanel stp = new StackPanel() { Margin = new Thickness(10.0, 10.0, 0.0, 0.0) };//Панелька, содержит текст и кружочек
                // Create a red Ellipse.
                Ellipse myEllipse = new Ellipse()
                {//Кружочек

                    Fill = new SolidColorBrush(clr),
                    StrokeThickness = 0,
                    Stroke = Brushes.Black,
                    Width = 10,
                    Height = 10,
                    Margin = new Thickness(70, -23.0, 0.0, 0.0)
                };

                Label lbl = new Label()
                {//Текст
                    Content = text,
                    Margin = new Thickness(0.0, -12.0, 0.0, 0.0)
                };
                stp.Children.Add(lbl);
                stp.Children.Add(myEllipse);
                label1Status_Copy.Content = stp;

 
            }));

        }

        private void CartStatus(int n, Brush clr)
        {
            this.Dispatcher.Invoke(new Action(delegate
            {

                ListViewItem item = new ListViewItem();
                item.Content = ((ListViewItem)this.listBox1.Items[n ]).Background = clr;
          
            })); 
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            PageSwitcher.PageSwitch(new Page1());
        }

        StackPanel stackPanel1;
        Button buttonCartWait;
        Button buttonCartProcessing;
        Button buttonCartSuccesful;
        Button buttonCartPanel_close;
        Button buttonCartSuccesfulAll;

        private void buttonCartWait_Click_1(object sender, RoutedEventArgs e)
        {
            grid1.Children.RemoveAt(grid1.Children.Count - 1);
            listBox1.IsEnabled = true;
            _server.SendMeesage(Encoding.GetEncoding(1251).GetBytes( "Ждите ваш заказ"), listBox1.SelectedIndex);
            //_server.SendMeesage(Encoding.GetEncoding(1251).GetBytes( AppData.ListToXML(new AppData.ListXMLOfType(7, new List<string>(){"0"}, Convert.ToString( listBox1.SelectedIndex)))), listBox1.SelectedIndex+1);
        }
     
        private void buttonCartProcessing_Click_1(object sender, RoutedEventArgs e)
        {
            grid1.Children.RemoveAt(grid1.Children.Count - 1);
            listBox1.IsEnabled = true;
            _server.SendMeesage(Encoding.GetEncoding(1251).GetBytes("Ваш заказ выполняется"), listBox1.SelectedIndex);
            //_server.SendMeesage(Encoding.GetEncoding(1251).GetBytes(AppData.ListToXML(new AppData.ListXMLOfType(7, new List<string>() { "1" }, Convert.ToString(listBox1.SelectedIndex)))), listBox1.SelectedIndex + 1);
        }

        private void buttonCartSuccesful_Click_1(object sender, RoutedEventArgs e)
        {
            grid1.Children.RemoveAt(grid1.Children.Count - 1);
            listBox1.IsEnabled = true;
            _server.SendMeesage(Encoding.GetEncoding(1251).GetBytes("Запрошенный вами позиция приготовлена "), listBox1.SelectedIndex);
            CartStatus(listBox1.SelectedIndex, Brushes.White);
           
        }
        private void buttonCartSuccesfulAll_Click_1( object sender, RoutedEventArgs e )
        {
            grid1.Children.RemoveAt( grid1.Children.Count - 1 );
            listBox1.IsEnabled = true;
            _server.SendMeesage( Encoding.GetEncoding( 1251 ).GetBytes( "Ваш заказ, приготовлен, ждите" ), listBox1.SelectedIndex );
            CartStatus( listBox1.SelectedIndex, Brushes.White );
            clientsAndCarts[listBox1.SelectedIndex].Clear();
            richTextBox1Orders.Document.Blocks.Clear();
        }

        private void listBox1_MouseLeftButtonUp_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            richTextBox1Orders.Document.Blocks.Clear();
            CartToDisplay ( Convert.ToString(listBox1.SelectedIndex), clientsAndCarts[listBox1.SelectedIndex], richTextBox1Orders);//отображаем заказ в нужном нам поле вывода
        }

        void buttonCartPanel_close_Click( object sender, RoutedEventArgs e )
        {
            stackPanel1.Children.Clear();
            grid1.Children.RemoveAt( grid1.Children.Count - 1 );
            listBox1.IsEnabled = true;
           
        }
        private void listBox1_MouseDoubleClick_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {


            if (clientsAndCarts[listBox1.SelectedIndex].Count()!=0)
            {
                buttonCartWait = new Button()
                {
                    Height = 40,
                    Background = Brushes.White,
                    Content = "Блюдо в очереди"

                };

                buttonCartProcessing = new Button()
                {
                    Height = 40,
                    Background = Brushes.White,
                    Content = "Блюдо готовится"

                };

                buttonCartSuccesful = new Button()
                {

                    Height = 40,
                    Background = Brushes.White,
                    Content = "готовность блюда "

                };

                buttonCartSuccesfulAll = new Button()
                {

                    Height = 40,
                    Background = Brushes.White,
                    Content = "Готово весь заказ"

                };

                buttonCartPanel_close = new Button()
                {
                    Margin = new Thickness(0, 20, 0, 0),
                    Height = 20,
                    Background = Brushes.Red,
                    Content = "Закрыть"
                    ,
                    Opacity = 1

                };

                buttonCartWait.Click += buttonCartWait_Click_1;
                buttonCartProcessing.Click += buttonCartProcessing_Click_1;
                buttonCartSuccesful.Click += buttonCartSuccesful_Click_1;
                buttonCartPanel_close.Click += buttonCartPanel_close_Click;
                buttonCartSuccesfulAll.Click += buttonCartSuccesfulAll_Click_1;
                stackPanel1 = new StackPanel()
                {
                    Background = Brushes.LightGray,
                   // Height = 200,
                    Margin = new Thickness(15, 30, 0, 0),
                    VerticalAlignment = System.Windows.VerticalAlignment.Top,
                    Width = 136,
                    Opacity = 0.85
                };
                buttonCartProcessing.IsEnabled = true;
                buttonCartWait.IsEnabled = true;
                buttonCartSuccesful.IsEnabled = true;
                buttonCartPanel_close.IsEnabled = true;
                buttonCartSuccesfulAll.IsEnabled = true;

                buttonCartProcessing.Visibility = System.Windows.Visibility.Visible;
                buttonCartWait.Visibility = System.Windows.Visibility.Visible;
                buttonCartSuccesful.Visibility = System.Windows.Visibility.Visible;
                buttonCartPanel_close.Visibility = Visibility.Visible;
                buttonCartSuccesfulAll.Visibility = System.Windows.Visibility.Visible;

                Int32 id = listBox1.SelectedIndex;

                stackPanel1.Children.Add(buttonCartWait);
                stackPanel1.Children.Add(buttonCartProcessing);
                stackPanel1.Children.Add(buttonCartSuccesful);
                stackPanel1.Children.Add( buttonCartPanel_close );

                grid1.Children.Add(stackPanel1);

                listBox1.IsEnabled = false;

            }
        }
       



    }
}
